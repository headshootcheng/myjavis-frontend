import React, { useEffect } from "react";
import firebase from "../firebase.config";
import { Route, useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { fetchUser } from "../redux/actions/auth";
import { useAuthState } from "react-firebase-hooks/auth";
const PrivateRoute = ({ path, component }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [user, loading, error] = useAuthState(firebase.auth());
  useEffect(() => {
    if (user) {
      dispatch(fetchUser(user.uid));
    } else if (loading) {
      console.log("loading");
    } else {
      console.log("no user");
      history.push("/auth");
    }
  }, [user, history, dispatch, loading]);
  return <Route exact path={path} component={component} />;
};
export default PrivateRoute;
