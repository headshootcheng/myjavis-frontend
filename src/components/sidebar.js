import React, { useState } from "react";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import HomeIcon from "@material-ui/icons/Home";
import WebIcon from "@material-ui/icons/Web";
import { useTranslation } from "react-i18next";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import WorkIcon from "@material-ui/icons/Work";
import Collapse from "@material-ui/core/Collapse";

const SideMenu = ({ onCloseMenu, isMenuOpen, switchPage }) => {
  const { t } = useTranslation();
  const [isManageWebOpen, setManageWebOpen] = useState(false);

  const toggleManageWebOpen = () => {
    setManageWebOpen((prevState) => !prevState);
  };

  const list = (onCloseMenu, switchPage) => {
    return (
      <div onKeyDown={onCloseMenu} className="w-56">
        <ListItem button onClick={() => switchPage("home")} className="mt-2">
          <ListItemIcon>
            <HomeIcon />
          </ListItemIcon>
          <ListItemText primary={t("menu.home")} />
        </ListItem>
        {/* <ListItem
          button
          onClick={() => switchPage("taskCreate")}
          className="mt-2"
        >
          <ListItemIcon>
            <AddCircleIcon />
          </ListItemIcon>
          <ListItemText primary={t("menu.taskCreate")} />
        </ListItem> */}
        <ListItem button onClick={() => toggleManageWebOpen()}>
          <ListItemIcon>
            <WebIcon />
          </ListItemIcon>
          <ListItemText primary={t("menu.manageMyWeb")} />
          {isManageWebOpen ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={isManageWebOpen} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItem button>
              <ListItemIcon>
                <WorkIcon />
              </ListItemIcon>
              <ListItemText primary={t("menu.manageMyProject")} />
            </ListItem>
          </List>
        </Collapse>
      </div>
    );
  };

  return (
    <Drawer anchor="left" open={isMenuOpen} onClose={onCloseMenu}>
      {list(onCloseMenu, switchPage)}
    </Drawer>
  );
};

export default SideMenu;
