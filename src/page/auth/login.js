import React, { useState, useEffect } from "react";
import { TextField, Button } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";
import { loginAction } from "../../redux/actions/auth";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { useAuthState } from "react-firebase-hooks/auth";
import firebase from "../../firebase.config";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { t } = useTranslation();
  const dispatch = useDispatch();
  const history = useHistory();
  const [user, loading, error] = useAuthState(firebase.auth());

  const authMessageBox = useSelector((state) => state.authMessageBox);

  const renderMessageBar = () => {
    const { type, message } = authMessageBox;
    //console.log("re-render MessageBar");
    switch (type) {
      default:
        return null;
      case "success":
        return <Alert severity="success">{t(message)}</Alert>;
      case "error":
        return <Alert severity="error">{t(message)}</Alert>;
    }
  };

  useEffect(() => {
    if (user) {
      history.push("/");
    }
    if (loading) {
      console.log("loading");
    }
  }, [history, user, loading]);

  const login = (e) => {
    e.preventDefault();
    dispatch(loginAction(email, password));
  };

  const validateInput = () => {
    const emailWithoutSpace = email.trim();
    const passwordWithoutSpace = password.trim();
    if (emailWithoutSpace !== "" && passwordWithoutSpace !== "") {
      return false;
    }
    return true;
  };

  return (
    <form
      className=" flex flex-1 flex-col mx-4 my-4 "
      onSubmit={login}
      noValidate
    >
      <div className="h-40">
        {renderMessageBar()}
        <TextField
          className=" w-11/12"
          required
          id="email"
          label={t("auth.email")}
          placeholder={t("auth.enterEmail")}
          value={email}
          onChange={(event) => {
            setEmail(event.target.value);
          }}
        />
        <br />
        <TextField
          className=" w-11/12"
          required
          id="password"
          label={t("auth.password")}
          placeholder={t("auth.enterPassword")}
          type="password"
          value={password}
          onChange={(event) => {
            setPassword(event.target.value);
          }}
        />
      </div>
      <br />
      <br />
      <Button
        variant="contained"
        color="secondary"
        type="submit"
        disabled={validateInput()}
      >
        {t("auth.login")}
      </Button>
    </form>
  );
};

export default Login;
