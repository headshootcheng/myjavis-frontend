export const REGISTER_USER = "REGISTER";
export const LOGIN = "LOGIN";
export const LOGINON = "LOGINON";
export const LOGINOFF = "LOGINOFF";
export const FETCH_USER = "FETCH_USER";
export const SHOW_ERROR_BOX = "SHOW_ERROR";
export const SHOW_SUCCESS_BOX = "SHOW_SUCCESS";
export const CLEAR_BOX = "CLEAR_BOX";

export const registerUserAction = (username, email, password) => {
  return {
    type: REGISTER_USER,
    content: { username, email, password },
  };
};

export const loginAction = (email, password) => {
  return {
    type: LOGIN,
    content: { email, password },
  };
};

export const loginOn = (body) => {
  return {
    type: LOGINON,
    body,
  };
};

export const loginOff = () => {
  return {
    type: LOGINOFF,
  };
};

export const fetchUser = (uid) => {
  return {
    type: FETCH_USER,
    uid,
  };
};

export const showAuthErrorBox = (msg) => {
  return {
    type: SHOW_ERROR_BOX,
    content: msg,
  };
};

export const showAuthSuccessBox = (msg) => {
  return {
    type: SHOW_SUCCESS_BOX,
    content: msg,
  };
};

export const clearAuthBox = () => {
  return {
    type: CLEAR_BOX,
  };
};
