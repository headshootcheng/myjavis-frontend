import { combineReducers } from "redux";
import userInfo from "./auth/userInfo";
import authMessageBox from "./auth/authMessageBox";
const rootReducer = combineReducers({
  userInfo,
  authMessageBox,
});

export default rootReducer;
