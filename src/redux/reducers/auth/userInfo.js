import { LOGINON, LOGINOFF } from "../../actions/auth";

const initStatus = { isLoggedIn: false, user: null };

const userInfo = (state = initStatus, actions) => {
  switch (actions.type) {
    case LOGINON: {
      return {
        isLoggedIn: true,
        user: actions.body,
      };
    }
    case LOGINOFF: {
      return { isLoggedIn: false, user: null };
    }
    default: {
      return state;
    }
  }
};

export default userInfo;
