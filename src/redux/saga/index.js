import { LOGIN, REGISTER_USER, LOGINOFF, FETCH_USER } from "../actions/auth";
import { all, takeLatest } from "redux-saga/effects";

import { loginSaga, registerSaga, fetchUserSaga, logoutSaga } from "./auth";

export default function* sagaWatcher() {
  yield all([
    takeLatest(LOGIN, loginSaga),
    takeLatest(REGISTER_USER, registerSaga),
    takeLatest(LOGINOFF, logoutSaga),
    takeLatest(FETCH_USER, fetchUserSaga),
  ]);
}
