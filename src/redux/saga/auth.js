import { call, put } from "redux-saga/effects";
import authService from "../../service/auth";
import {
  loginOn,
  showAuthErrorBox,
  clearAuthBox,
  showAuthSuccessBox,
} from "../actions/auth";
export function* loginSaga(action) {
  try {
    yield call(authService.login, action.content);
    yield put(clearAuthBox());
  } catch (error) {
    yield put(showAuthErrorBox(error.code));
  }
}

export function* registerSaga(action) {
  try {
    const { user } = yield call(authService.registerUser, action.content);
    yield call(authService.saveNewUser, { uid: user.uid, ...action.content });
    yield put(showAuthSuccessBox("register Success"));
  } catch (error) {
    yield put(showAuthErrorBox(error.code));
  }
}

export function* fetchUserSaga({ uid }) {
  try {
    const data = yield call(authService.getUserInfo, uid);
    yield put(loginOn(data));
  } catch (error) {
    console.log("fetch user Saga error: ", error);
  }
}

export function* logoutSaga() {
  try {
    yield call(authService.logout);
  } catch (error) {
    console.log("logout error", error);
  }
}
