import firebase from "../firebase.config";
const authService = {
  registerUser: async ({ email, password }) => {
    const data = await firebase
      .auth()
      .createUserWithEmailAndPassword(email, password);
    return data;
  },
  login: async ({ email, password }) => {
    const data = await firebase
      .auth()
      .signInWithEmailAndPassword(email, password);
    return data;
  },
  logout: () => {
    firebase.auth().signOut();
  },
  saveNewUser: ({ uid, username, email }) => {
    firebase
      .database()
      .ref("users/" + uid)
      .set({
        uid,
        username,
        email,
      });
  },

  getUserInfo: async (uid) => {
    const userRef = firebase.database().ref("users/" + uid);
    const data = await userRef.once("value");
    return data.val();
  },
};

export default authService;
