import React, { lazy, Suspense } from "react";
import ReactDOM from "react-dom";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider } from "react-redux";
import { applyMiddleware, createStore, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./redux/saga";
import rootReducer from "./redux/reducers";
import "./i18n";
import "./style/output.css";
import { devToolsEnhancer } from "redux-devtools-extension";
import PrivateRoute from "./components/privateRoute";

const sagaMiddleWare = createSagaMiddleware();
const store = createStore(
  rootReducer,
  compose(applyMiddleware(sagaMiddleWare), devToolsEnhancer({ trace: true }))
);
sagaMiddleWare.run(rootSaga);
const Auth = lazy(() => import("./page/auth"));
const Home = lazy(() => import("./page/home"));
ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Suspense fallback={<div>Loading ...</div>}>
        <Switch>
          <Route path="/auth" component={Auth} />
          <PrivateRoute path="/" component={Home} />
        </Switch>
      </Suspense>
    </Router>
  </Provider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
