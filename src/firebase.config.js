import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/database";
const config = {
  apiKey: "AIzaSyDWt_vA-QA4F1uJj7QcT7sKlXg6mTMlmi4",
  authDomain: "myjarvis-d37ec.firebaseapp.com",
  databaseURL:
    "https://myjarvis-d37ec-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "myjarvis-d37ec",
  storageBucket: "myjarvis-d37ec.appspot.com",
  messagingSenderId: "229141675303",
  appId: "1:229141675303:web:afe57bb3b6cd62aa50fd02",
};
firebase.initializeApp(config);
export default firebase;
